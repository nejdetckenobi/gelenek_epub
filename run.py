from crawlers import get_vol, get_vol_count
from builders import create_epub_archive, create_vol_directory
from shutil import rmtree
from os import makedirs, listdir


if __name__ == '__main__':
    try:
        vol_count = get_vol_count()
        print(vol_count, 'volumes to fetch.')
        try:
            makedirs('epubs')
        except FileExistsError:
            pass
        epubs = listdir('epubs')
        for i in range(1, vol_count + 1):
            if '{}.epub'.format(i) in epubs:
                print('GELENEK VOL {} ALREADY EXISTS. SKIPPED.'.format(i))
                continue
            v = get_vol(i)
            create_vol_directory(v)
            create_epub_archive('epubs/{}.epub'.format(i))
            rmtree('temp')
            print('GELENEK VOL {} EPUB CREATED'.format(i))
    except Exception as e:
        try:
            rmtree('temp')
        except Exception as e:
            print(e.msg)
