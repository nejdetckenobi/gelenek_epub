# GELENEK - EPUB

Bu proje Gelenek yayınının arşivindeki tüm sayıları bilgisayarınıza EPUB formatında çeker.


## Bağımlılıklar

Çalıştırmak için Python3 gerekiyor.
Şu anda 3.5.2 sürümünde çalıştığını biliyorum.
Bundan daha düşük sürümler için çalışabilir ancak garantisi yok.

Python kütüphaneleri bazında bağımlılıkları görmek için `requirements.txt` dosyasına bakabilirsiniz.
Bir ek geldiğinde iki yeri birden güncellemek gerekmesin diye buraya yazmıyorum.


## Hiyerarşi

Proje aşağıdaki klasör yapısını kullanır

```
.
├── builders.py
├── crawlers.py
├── env
├── temp
├── style.css
├── meta.epub
├── README.md
├── requirements.txt
└── run.py
```

Bu yapıda,

- `meta.epub` dosyası örnek bir EPUB dokümanı,
- `temp` dizini geçici klasör
- `README.md` şu anda okuduğunuz doküman

olup, bunlar projeyi çalıştırmak için gerekli değildir. Silinebilir.

- `env` dizini tarafınızdan oluşturulacak sanal ortamı (kurulum kısmında bunu oluşturan bir madde var.)
- `builders.py` dosyası EPUB oluşturma fonksiyonlarını,
- `crawlers.py` dosyası arşivdeki verilerin çekilmesi ve ayrıştırılması için gereken fonksiyonları,
- `run.py` projenin çalıştırma kodlarını,
- `requirements.txt` dosyası projenin bağımlı olduğu python kütüphanelerinin listesini,
- `style.css` dosyası her bir EPUB dosyası için kullanılacak olan biçimlendirme kurallarını,

içerir ve projenin çalışması için gereklidir. Silinmemelidirler.


## Kurulum ve çalıştırma

- Bilgisayarınızda en azından `Python 3.5.2` olduğundan ve içinde `virtualenv` kurulu olduğundan emin olun.
- Projeyi klonlayın: `git clone git@bitbucket.org:nejdetckenobi/gelenek_epub.git`
- Projenin bulunduğu dizine gidin: `cd gelenek_epub`
- Bir sanal Python ortamı oluşturun: `virtualenv -p "$(which python)" env`
- Sanal ortamı aktifleştirin: `source env/bin/activate`
- Sanal ortam içine bağımlılıkları yükleyin: `pip install -r requirements.txt`
- Projeyi çalıştırın: `python run.py`

Projenin çalışması bittiğinde `epubs/` dizini altında e-kitap'larınızı bulabilirsiniz.


## Notlar

- Proje, Gelenek yayınının erişime açık kısmının EPUB formatına çevrilmesi amacından başka bir amaç taşımaz.
- Proje, yayını çıkaran ekiple bağlantısız olup, ayrıca geliştirilmektedir.
- Projenin çalışması sonucu oluşabilecek herhangi sorun, yayını çıkaran ekibi bağlamaz, bu sorunlar depo sahibine iletilmelidir.
- Projede, yayının içeriği değiştirilmemektedir.
  Yayının orijinali ile projenin çıkardığı formatların tutarsız olması halinde yayının orijinali geçerlidir.
  Bu durumla karşılaşılırsa, mümkünse issue oluşturularak belirtilmelidir.
- Proje kullanılacaksa depo kaynak gösterilmelidir.
- Proje geliştirme altında olduğundan, EPUB dosyaları paylaşılmamıştır.
  Görünüm iyileştirilmeleri yapılmaktadır.
  Bu sebeple EPUB'lar şimdilik her seferinde baştan üretilmelidir.
  Mevcut görünüm tarzının değiştirilmesine olanak sağlayan scriptler proje hiyerarşisinde `patches/` isimli bir dizin altında paylaşılacaktır.


## Bilinen sorunlar

- 5 numaralı sayı için oluşturulan EPUB dosyasında kapak resminin olmaması bilinen bir durumdur. Arşivde düzeltilmesi halinde kendiliğinden düzelecektir.
- Thread henüz kullanılmamıştır. Yakın zamanda eklenecektir.
