from xmltodict import unparse
from zipfile import ZipFile
from os import mkdir, listdir
from collections import OrderedDict
from crawlers import BASE_URL


def make_xml_from_article(article):
    data = OrderedDict((
        ('html', OrderedDict((
            ('@xmlns', 'http://www.w3.org/1999/xhtml'),
            ('@xml:lang', 'tr'),
            ('head', OrderedDict((
                ('title', article['title']),
                ('meta', OrderedDict((
                    ('@http-equiv', 'Content-Type'),
                    ('@content', 'application/xhtml+xml; charset=utf-8'),
                ))),
                ('link', OrderedDict((
                    ('@rel', 'stylesheet'),
                    ('@href', '../assets/style.css'),
                    ('@type', 'text/css'),
                ))),
            ))),
            ('body', OrderedDict((
                ('h2', OrderedDict((
                    ('#text', article['title']),
                    ('@class', 'article-title'),
                ))),
                ('p', [{'@class': 'article-paragraph', '#text': paragraph}
                       for paragraph in article['data']]),
            ))),
        ))),
    ))
    return unparse(data, pretty=True, short_empty_elements=True)


def slug_from_link(link):
    return 'makale-' + link.split('/')[-1]


def create_container_xml_data():
    data = OrderedDict((
        ('container', OrderedDict((
            ('@version', '1.0'),
            ('@xmlns', 'urn:oasis:names:tc:opendocument:xmlns:container'),
            ('rootfiles', OrderedDict((
                ('rootfile', OrderedDict((
                    ('@full-path', 'book.opf'),
                    ('@media-type', 'application/oebps-package+xml'),
                ))),
            )))
        ))),
    ))
    return unparse(data, pretty=True, short_empty_elements=True)


def create_toc_nxc_data(vol):
    nav_points = []
    nav_points.append(OrderedDict((
        ('@id', 'cover-image'),
        ('@playOrder', 1),
        ('navLabel', OrderedDict((
            ('text', 'Kapak'),
        ))),
        ('content', OrderedDict((
            ('@src', 'OEPBS/front-cover.html'),
        ))),
    )))

    nav_points.append(OrderedDict((
        ('@id', 'table-of-contents'),
        ('@playOrder', 2),
        ('navLabel',  OrderedDict((
            ('text', 'İçindekiler'),
        ))),
        ('content', OrderedDict((
            ('@src', 'OEPBS/table-of-contents.html'),
        ))),
    )))

    for i, article in enumerate(vol['articles']):
        nav_points.append(OrderedDict((
            ('@id', slug_from_link(article['link'])),
            ('@playOrder', i + 3),
            ('navLabel',  OrderedDict((
                ('text', article['title']),
            ))),
            ('content', OrderedDict((
                ('@src', 'OEPBS/articles/' + slug_from_link(article['link']) + '.html'),  # NOQA
            )))
        )))

    data = OrderedDict((
        ('ncx', OrderedDict((
            ('@version', '2005-1'),
            ('@xml:lang', 'tr'),
            ('@xmlns', 'http://www.daisy.org/z3986/2005/ncx/'),
            ('head', OrderedDict((
                ('meta', [
                    OrderedDict((
                        ('@name', 'dtb:uid'),
                        ('@content', BASE_URL + 'sayi/' + str(vol['no'])),
                    )),
                    OrderedDict((
                        ('@name', 'dtb:depth'),
                        ('@content', '2'),
                    )),
                    OrderedDict((
                        ('@name', 'dtb:totalPageCount'),
                        ('@content', '0'),
                    )),
                    OrderedDict((
                        ('@name', 'dtb:maxPageNumber'),
                        ('@content', '0'),
                    )),
                ]),
            ))),
            ('docTitle', OrderedDict((
                ('text', 'Gelenek - ' + str(vol['no'])),
            ))),
            ('docAuthor', OrderedDict((
                ('text', 'TKP'),
            ))),
            ('navMap', OrderedDict((
                ('navPoint', nav_points),
            )))
        ))),
    ))
    return unparse(data, pretty=True, short_empty_elements=True)


def create_book_opf_data(vol):
    items = []
    item_refs = []
    items.append(OrderedDict((
        ('@id', 'front-cover'),
        ('@href', 'OEPBS/front-cover.html'),
        ('@media-type', 'application/xhtml+xml'),
    )))
    items.append(OrderedDict((
        ('@id', 'table-of-contents'),
        ('@href', 'OEPBS/table-of-contents.html'),
        ('@media-type', 'application/xhtml+xml'),
    )))
    item_refs.append(OrderedDict((
        ('@idref', 'front-cover'),
        ('@linear', 'no'),
    )))
    item_refs.append(OrderedDict((
        ('@idref', 'table-of-contents'),
        ('@linear', 'yes'),
    )))
    for article in vol['articles']:
        items.append(OrderedDict((
            ('@id', slug_from_link(article['link'])),
            ('@href', 'OEPBS/articles/' + slug_from_link(article['link']) + '.html'),  # NOQA
            ('@media-type', 'application/xhtml+xml'),
        )))
        item_refs.append(OrderedDict((
            ('@idref', slug_from_link(article['link'])),
            ('@linear', 'yes'),
        )))
    items.append(OrderedDict((
        ('@id', 'cover-image'),
        ('@href', 'OEPBS/assets/cover.png',),
        ('@media-type', 'image/png'),
    )))
    items.append(OrderedDict((
        ('@id', 'ncx'),
        ('@href', 'toc.ncx',),
        ('@media-type', 'application/x-dtbncx+xml'),
    )))
    items.append(OrderedDict((
        ('@id', 'stylesheet'),
        ('@href', 'OEPBS/assets/style.css'),
        ('@media-type', 'text/css'),
    )))

    data = OrderedDict((
        ('package', OrderedDict((
            ('@version', '2.0'),
            ('@xmlns', 'http://www.idpf.org/2007/opf'),
            ('@unique-identifier', 'PrimaryID'),
            ('metadata', OrderedDict((
                ('@xmlns:dc', 'http://purl.org/dc/elements/1.1/'),
                ('@xmlns:opf', 'http://www.idpf.org/2007/opf'),
                ('dc:title', vol['title']),
                ('dc:language', 'tr'),
                ('dc:identifier', OrderedDict((
                    ('@id', 'PrimaryID'),
                    ('@opf:scheme', 'URI'),
                    ('#text', BASE_URL + '/sayi/' + str(vol['no'])),
                ))),
                ('dc:description', 'BURAYA_ACIKLAMA_GELECEK'),
                ('dc:creator', OrderedDict((
                    ('@opf:role', 'aut'),
                    ('#text', 'TKP'),
                ))),
                ('dc:publisher', 'TKP'),
            ))),
            ('manifest', OrderedDict((
                ('item', items),
            ))),
            ('spine', OrderedDict((
                ('@toc', 'ncx'),
                ('itemref', item_refs),
            ))),
            ('guide', OrderedDict((
                ('reference', [
                    OrderedDict((
                        ('@type', 'toc'),
                        ('@title', 'İçindekiler'),
                        ('@href', 'OEPBS/table-of-contents.html'),
                    )),
                    OrderedDict((
                        ('@type', 'cover'),
                        ('@title', 'cover'),
                        ('@href', 'OEPBS/front-cover.html'),
                    )),
                ]),
            ))),
        ))),
    ))

    return unparse(data, pretty=True, short_empty_elements=True)


def create_table_of_contents_html_data(vol):
    lis = []
    for article in vol['articles']:
        lis.append(OrderedDict((
            ('a', OrderedDict((
                ('@href', 'articles/' + slug_from_link(article['link']) + '.html'),  # NOQA
                ('span', article['title']),
            ))),
        )))

    data = OrderedDict((
        ('html', OrderedDict((
            ('@xmlns', 'http://www.w3.org/1999/xhtml'),
            ('@xml:lang', 'tr'),
            ('head', OrderedDict((
                ('title', 'İçindekiler'),
                ('meta', [
                    OrderedDict((
                        ('@http-equiv', 'Content-Type'),
                        ('@content', 'application/xhtml+xml; charset=utf-8'),
                    )),
                    OrderedDict((
                        ('@name', 'EPB-UUID'),
                        ('@content', ''),
                    )),
                ]),
            ))),
            ('body', OrderedDict((
                ('div', OrderedDict((
                    ('@id', 'toc'),
                    ('h1', 'İçindekiler'),
                    ('ul', OrderedDict((
                        ('li', lis),
                    ))),
                ))),
            ))),
        ))),
    ))
    return unparse(data, pretty=True, short_empty_elements=True)


def create_front_cover_html(vol):
    data = OrderedDict((
        ('html', OrderedDict((
            ('@xmlns', 'http://www.w3.org/1999/xhtml'),
            ('@xml:lang', 'tr'),
            ('head', OrderedDict((
                ('title', vol['title']),
                ('meta', [
                    OrderedDict((
                        ('@http-equiv', 'Content-Type'),
                        ('@content', 'application/xhtml+xml; charset=utf-8'),
                    )),
                    OrderedDict((
                        ('@name', 'EPB-UUID'),
                        ('@content', ''),
                    ))
                ]),
            ))),
            ('body', OrderedDict((
                ('div', OrderedDict((
                    ('@id', 'cover-image'),
                    ('img', OrderedDict((
                        ('@src', 'assets/cover.png'),
                        ('@alt', vol['title']),
                    ))),
                ))),
            ))),
        ))),
    ))
    return unparse(data, pretty=True, short_empty_elements=True)


def create_vol_directory(vol):
    mkdir('temp')
    with open('temp/mimetype', 'w') as mimetypefile:
        mimetypefile.write('application/epub+zip')
    with open('temp/toc.ncx', 'w') as toc_ncx:
        toc_ncx.write(create_toc_nxc_data(vol))
    with open('temp/book.opf', 'w') as book_opf:
        book_opf.write(create_book_opf_data(vol))
    mkdir('temp/META-INF')
    with open('temp/META-INF/container.xml', 'w') as container_xml:
        container_xml.write(create_container_xml_data())
    mkdir('temp/OEPBS')
    with open('temp/OEPBS/front-cover.html', 'w') as front_cover:
        front_cover.write(create_front_cover_html(vol))
    with open('temp/OEPBS/table-of-contents.html', 'w') as table_of_contents:
        table_of_contents.write(create_table_of_contents_html_data(vol))
    mkdir('temp/OEPBS/assets')
    with open('temp/OEPBS/assets/cover.png', 'wb') as cover_image:
        cover_image.write(vol['image'])
    with open('style.css') as style_source:
        style_data = style_source.read()
    with open('temp/OEPBS/assets/style.css', 'w') as style_css:
        style_css.write(style_data)
    mkdir('temp/OEPBS/articles')
    for article in vol['articles']:
        with open('temp/OEPBS/articles/{}.html'.format(
                slug_from_link(article['link'])), 'w') as article_doc:
            article_doc.write(make_xml_from_article(article))


def create_epub_archive(path):
    z = ZipFile(path, 'w')
    z.write('temp/mimetype', 'mimetype')
    z.write('temp/toc.ncx', 'toc.ncx')
    z.write('temp/OEPBS', 'OEPBS')
    z.write('temp/OEPBS/front-cover.html', 'OEPBS/front-cover.html')
    z.write('temp/OEPBS/table-of-contents.html',
            'OEPBS/table-of-contents.html')
    z.write('temp/OEPBS/articles', 'OEPBS/articles')
    for each in listdir('temp/OEPBS/articles'):
        z.write('temp/OEPBS/articles/' + each, 'OEPBS/articles/' + each)
    z.write('temp/OEPBS/assets', 'OEPBS/assets')
    for each in listdir('temp/OEPBS/assets'):
        z.write('temp/OEPBS/assets/' + each, 'OEPBS/assets/' + each)
    z.write('temp/book.opf', 'book.opf')
    z.write('temp/META-INF', 'META-INF')
    z.write('temp/META-INF/container.xml', 'META-INF/container.xml')
    z.close()
