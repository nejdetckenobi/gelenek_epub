from pyquery import PyQuery as pq
import requests


BASE_URL = 'https://www.gelenek.org/'


def get_vol_count():
    response = requests.get(BASE_URL + 'arsiv')
    selector = pq(response.content)
    last_vol_link = selector('.gelenek-container a')[0].attrib.get('href')
    last_vol = int(last_vol_link.rsplit('/', 1)[1])
    return last_vol


def get_vol_selector(vol_number):
    response = requests.get(BASE_URL + 'sayi/' + str(vol_number))
    selector = pq(response.content)
    return selector


def get_vol_title(vol_selector):
    title = '{} - {}'.format(vol_selector('.single-issue-title').text(),
                             vol_selector('.single-issue-slogan').text())
    return title


def get_article_links(vol_selector):
    article_elements = vol_selector('.issue-content-title a')
    article_links = [e.attrib.get('href') for e in article_elements]
    return article_links


def get_article(link):
    response = requests.get(BASE_URL + link)
    selector = pq(response.content)
    title = selector('.makale-title a')[0].text
    author = selector('.makale-author')[0].text
    data = [p.text for p in selector('.makale-right p')]
    return {'title': title,
            'author': author,
            'data': data,
            'link': link}


def download_vol_image(vol_number):
    resp = requests.get(BASE_URL + 'sites/default/files/{:0>3}.png'.format(
        vol_number))

    i = 0
    while resp.status_code != 200:
        resp = requests.get(
            BASE_URL + 'sites/default/files/{:0>3}_{}.png'.format(
                vol_number, i
            )
        )
        i += 1
    return resp.content


def get_vol(vol_number):
    selector = get_vol_selector(vol_number)
    article_links = get_article_links(selector)
    return {
        'no': vol_number,
        'title': get_vol_title(selector),
        'articles': [get_article(link) for link in article_links],
        'image': download_vol_image(vol_number)
    }
